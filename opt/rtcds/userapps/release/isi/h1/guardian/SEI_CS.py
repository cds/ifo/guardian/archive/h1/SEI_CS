from sei_config.sensor_correction.states import *
from sei_config.sensor_correction.const import SC_FM_confs
from cdsutils import CDSMatrix
import numpy as np

prefix = 'SEI-CS'
nominal = 'CONFIG_WINDY'

########################################
# Constants

STS_TRAMP = 30
    
sts_matrix_engaged = np.matrix([[ 0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.,  0.],
                                [ 0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.,  0.],
                                [ 0.,  0.,  0.,  0.,  0.,  1.,  0.,  0.,  0.]])
# This is just a zero'd matrix, but might not be later, so it's here.
sts_matrix_disengaged = np.matrix([[ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
                                   [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.],
                                   [ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.]])

########################################
# State generators

def gen_ramp_sts_matrix(engage=False):
    class RAMP_STS_MATRIX(GuardState):
        """Ramp the input matrix for the SEIPROC CS SC path,
        to verify that the entire SC path is off/on.
        """
        request = False
        goto = True
        def main(self):
            self.sts_mtrx = CDSMatrix(prefix=':ISI-GND_STS_MTRX',
                                      rows={'X': 1,
                                            'Y': 2,
                                            'Z': 3
                                      },
                                      cols={'HAM2_X': 1,
                                            'HAM2_Y': 2,
                                            'HAM2_Z': 3,
                                            'ITMY_X': 4,
                                            'ITMY_Y': 5,
                                            'ITMY_Z': 6,
                                            'HAM5_X': 7,
                                            'HAM5_Y': 8,
                                            'HAM5_Z': 9
                                      },
                                      ramping=True)
            # The any is necessary because a list of bools is returned
            if any(self.sts_mtrx.is_ramping()):
                notify('matrix already ramping')
                # FIXME: do more, like below but not
                #while self.sts_mtrx.is_ramping():
                #    notify('Waiting for STS matrix to finish ramping before starting')
            if engage:
                self.sts_mtrx.put_matrix(sts_matrix_engaged)
            else:
                self.sts_mtrx.put_matrix(sts_matrix_disengaged)
            self.sts_mtrx.TRAMP = STS_TRAMP
            self.sts_mtrx.load()

        def run(self):
            if any(self.sts_mtrx.is_ramping()):
                notify('matrix still ramping')
            else:
                return True
        
    return RAMP_STS_MATRIX

########################################
# State definitions

# Even though these states will do the same thing, in order
# to keep the paths separate, we need to do this awkward thing.
configs = [config for config in SC_FM_confs if config != 'SC_OFF']
for config in configs:
    globals()['RAMP_ON_STS_MATRIX_{}'.format(config)] = gen_ramp_sts_matrix(engage=True)
RAMP_OFF_STS_MATRIX = gen_ramp_sts_matrix(engage=False)

# Change the engaging states to goto false
for config in configs:
    if 'ENGAGING_CONFIG_{}'.format(config) in globals():
        globals()['ENGAGING_CONFIG_{}'.format(config)].goto = False
if 'ENGAGING_CONFIG_SC_OFF' in globals():
    globals()['ENGAGING_CONFIG_SC_OFF'].goto = False
        
########################################
# Edges

for config in configs:
    edges.append(('RAMP_ON_STS_MATRIX_{}'.format(config), 'ENGAGING_CONFIG_{}'.format(config)))
edges.append(('RAMP_OFF_STS_MATRIX', 'ENGAGING_CONFIG_SC_OFF'))
